var FORMATMODULE, RECORDMODULE, SEARCHMODULE, UIDIALOGMODULE, UIMESSAGEMODULE;

/**
 * @NApiVersion 2.1
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */
define(['N/format', 'N/record', 'N/search', 'N/ui/dialog', 'N/ui/message'], runClient);

function runClient(format, record, search, uidialog, uimessage) {
	FORMATMODULE= format;
	RECORDMODULE= record;
	SEARCHMODULE= search;
	UIDIALOGMODULE= uidialog;
	UIMESSAGEMODULE= uimessage;
  
	var returnObj = {};
	// returnObj['pageInit'] = _pageInit;
	// returnObj['fieldChanged'] = _fieldChanged;
	// returnObj['postSourcing'] = _postSourcing;
	// returnObj['sublistChanged'] = _sublistChanged;
	// returnObj['lineInit'] = _lineInit;
	// returnObj['validateField'] = _validateField;
	// returnObj['validateLine'] = _validateLine;
	// returnObj['validateInsert'] = _validateInsert;
	// returnObj['validateDelete'] = _validateDelete;
	returnObj['saveRecord'] = _saveRecord;
	return returnObj;
}
    
/**
 * Function to be executed after page is initialized.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.mode - The mode in which the record is being accessed (create, copy, or edit)
 *
 * @since 2015.2
 */
function _pageInit(scriptContext) {

}

/**
 * Function to be executed when field is changed.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 * @param {string} scriptContext.fieldId - Field name
 * @param {number} scriptContext.lineNum - Line number. Will be undefined if not a sublist or matrix field
 * @param {number} scriptContext.columnNum - Line number. Will be undefined if not a matrix field
 *
 * @since 2015.2
 */
function _fieldChanged(scriptContext) {
//	alert('_fieldChanged ' + JSON.stringify(scriptContext));
}

/**
 * Function to be executed when field is slaved.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 * @param {string} scriptContext.fieldId - Field name
 *
 * @since 2015.2
 */
function _postSourcing(scriptContext) {

}

/**
 * Function to be executed after sublist is inserted, removed, or edited.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @since 2015.2
 */
function _sublistChanged(scriptContext) {

}

/**
 * Function to be executed after line is selected.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @since 2015.2
 */
function _lineInit(scriptContext) {
}

/**
 * Validation function to be executed when field is changed.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 * @param {string} scriptContext.fieldId - Field name
 * @param {number} scriptContext.lineNum - Line number. Will be undefined if not a sublist or matrix field
 * @param {number} scriptContext.columnNum - Line number. Will be undefined if not a matrix field
 *
 * @returns {boolean} Return true if field is valid
 *
 * @since 2015.2
 */
function _validateField(scriptContext) {
	// alert('_validateField ' + JSON.stringify(scriptContext));
	return true;
}

/**
 * Validation function to be executed when sublist line is committed.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @returns {boolean} Return true if sublist line is valid
 *
 * @since 2015.2
 */
function _validateLine(scriptContext) {
	// alert('_validateLine ' + JSON.stringify(scriptContext));
	return true;
}

/**
 * Validation function to be executed when sublist line is inserted.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @returns {boolean} Return true if sublist line is valid
 *
 * @since 2015.2
 */
function _validateInsert(scriptContext) {

}

/**
 * Validation function to be executed when record is deleted.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @param {string} scriptContext.sublistId - Sublist name
 *
 * @returns {boolean} Return true if sublist line is valid
 *
 * @since 2015.2
 */
function _validateDelete(scriptContext) {
	return true;
}

/**
 * Validation function to be executed when record is saved.
 *
 * @param {Object} scriptContext
 * @param {Record} scriptContext.currentRecord - Current form record
 * @returns {boolean} Return true if record is valid
 *
 * @since 2015.2
 */
function _saveRecord(scriptContext) {
	// alert('_saveRecord ' + JSON.stringify(scriptContext));
	let returnValue = true;
	const DoNotProcessItemFulfillCreatedFrom = [SEARCHMODULE.Type.VENDOR_RETURN_AUTHORIZATION]
	let bypassLotValidatiion = false;
	if (scriptContext.currentRecord.type == RECORDMODULE.Type.ITEM_FULFILLMENT) {
		let fromType = '';
		let createdFrom = scriptContext.currentRecord.getValue({ fieldId: 'createdfrom'});
		if (createdFrom) {
			let searchObj = SEARCHMODULE.create({
				type: SEARCHMODULE.Type.TRANSACTION,
				filters: ['internalid', 'is', createdFrom],
				columns: ['tranid']
			});
			fromType = searchObj.run().getRange({ start: 0, end: 1})[0].recordType;
		}
		bypassLotValidatiion = DoNotProcessItemFulfillCreatedFrom.includes(fromType);
		// alert(bypassLotValidatiion);
	}
	if (!bypassLotValidatiion) {
		returnValue = verifyLotsValidityOnItemLines(scriptContext);
	}
	return returnValue;
}

function verifyLotsValidityOnItemLines(scriptContext) {
	let currentRec = scriptContext.currentRecord;
	// Depending on the deployed record the sublist to process is item or component
	let detailSublistId = (currentRec.getSublist({ sublistId: 'item' }) || currentRec.getSublist({ sublistId: 'component' })).id;
	// Get the appropriate field that supports the inventory detail depending on the sublist
	let summaryFieldId = { item: 'inventorydetail', component: 'componentinventorydetail' }[detailSublistId];
	let lineCount = currentRec.getLineCount({ sublistId: detailSublistId });
	let nowDate = getNowDate();
	let invalidLots = [];
	for (let i = 0; i < lineCount; i++) {
            if (!currentRec.hasSublistSubrecord({ sublistId: detailSublistId, fieldId: summaryFieldId, line: i })) {
                continue;
            }
            currentRec.selectLine({
                sublistId: detailSublistId,
                line: i
            });
            verifyCurrentItemLineLotsValidity(currentRec, detailSublistId, summaryFieldId, nowDate, invalidLots, i);
        }
        if (invalidLots.length > 0) {
            UIDIALOGMODULE.alert({
                title: 'ERROR - One or more lots used in transaction are invalid:',
                message: invalidLots.join('<br>')
            });
            return false;
        }
	return true;
}

function verifyCurrentItemLineLotsValidity(currentRecord, sublistId, summaryFieldId, nowDate, invalidLots, componentLine) {
        let item;
        let inventDetailRec = currentRecord.getCurrentSublistSubrecord({
            sublistId: sublistId,
            fieldId: summaryFieldId
        });
        let inventoryLineCount = inventDetailRec.getLineCount({ sublistId: 'inventoryassignment' });
        if (inventoryLineCount == 0) {
            return;
        }
        // Check lot expiration dates and expired/quarantine status for Work Order Issues
        for (let i = 0; i < inventoryLineCount; i++) {
            let inventoryNumber;
            if (currentRecord.type === RECORDMODULE.Type.WORK_ORDER_ISSUE) {
                let statusId = inventDetailRec.getSublistValue({
                    sublistId: 'inventoryassignment',
                    fieldId: 'inventorystatus',
                    line: i
                });
                if (statusId) {
                    let avoidAllocationWOIssuefieldsValues = SEARCHMODULE.lookupFields({
                        type: SEARCHMODULE.Type.INVENTORY_STATUS,
                        id: statusId,
                        columns: ['custrecord_bwp_avoidallocationwoissue']
                    });
                    // Check that lot is not expired or in quarantine
                    if (avoidAllocationWOIssuefieldsValues.custrecord_bwp_avoidallocationwoissue) {
                        if (!item) {
                            item = getItem(currentRecord, sublistId, componentLine);
                        }
                        // Inventory Number can't already be set
                        inventoryNumber = getInventoryNumber(inventDetailRec, i);
                        // The status label is searched for display only
                        let fieldsValues = SEARCHMODULE.lookupFields({
                            type: SEARCHMODULE.Type.INVENTORY_STATUS,
                            id: statusId,
                            columns: ['name']
                        });
                        let statusLabel = fieldsValues.name;
                        invalidLots.push(`Item ${item} / Lot ${inventoryNumber} / Status ${statusLabel}`);
                    }
                }
            }
            let expirDate = inventDetailRec.getSublistValue({
                sublistId: 'inventoryassignment',
                fieldId: 'expirationdate',
                line: i
            });
            if (expirDate && expirDate < nowDate) {
                let expirDateString = FORMATMODULE.format({
                    value: expirDate,
                    type: FORMATMODULE.Type.DATE
                });
                if (!item) {
                    item = getItem(currentRecord, sublistId, componentLine);
                }
                if (!inventoryNumber) {
                    inventoryNumber = getInventoryNumber(inventDetailRec, i);
                }
                invalidLots.push(`Item ${item} / Lot ${inventoryNumber} / Expiration date ${expirDateString}`);
            }
        }
    }
    /**
     * Get the forbidden lot component.
     *
     * @param currentRecord
     * @param sublistId
     * @returns
     */
    function getItem(currentRecord, sublistId, line) {
        let itemId = currentRecord.getSublistValue({
            sublistId: sublistId,
            fieldId: 'item',
            line: line
        });
        let fieldsValues = SEARCHMODULE.lookupFields({
            type: SEARCHMODULE.Type.INVENTORY_ITEM,
            id: itemId,
            columns: ['itemid']
        });
        return fieldsValues.itemid;
    }
/**
     * Get the inventory number of the forbidden lot.
     *
     * @param inventDetailRec
     * @param i
     * @returns
     */
function getInventoryNumber(inventDetailRec, i) {
  // getSublistText not supported on subrecord ... GRRRRR
  let inventoryNumberId = inventDetailRec.getSublistValue({
    sublistId: 'inventoryassignment',
    fieldId: 'issueinventorynumber',
    line: i
  });
  // ==> retrieve the inventory number display value thru the search module
  let fieldsValues = SEARCHMODULE.lookupFields({
    type: SEARCHMODULE.Type.INVENTORY_NUMBER,
    id: inventoryNumberId,
    columns: ['inventorynumber']
  });
  return fieldsValues.inventorynumber;
}

/**
 * Returns today's date with time 0
 * @returns {Date}
 */
function getNowDate() {
//	return new Date('2021/10/30');
//	return new Date('2021/11/01');
	let nowDateTime = new Date(Date.now());
	return new Date(nowDateTime.getFullYear(), nowDateTime.getMonth(), nowDateTime.getDate(), 0, 0, 0);
}

function logRecord(logTitle, record) {
	var logRecord = JSON.stringify(record) || 'UNDEFINED';
	var recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);		
	}
}

function logVar(title, value) {
	log.debug({
		title: title + ' (' + typeof value + ')',
		details: value 
	});	
}
